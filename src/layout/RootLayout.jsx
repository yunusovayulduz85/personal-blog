import React, { useState } from 'react'
import { Link, Outlet } from 'react-router-dom'
import Square from "../assets/square.png"
import { Bars3BottomRightIcon, XMarkIcon } from "@heroicons/react/24/solid"
const RootLayout = () => {
    const Links = [
        { name: "ABOUT ME", link: "/" },
        { name: "RESUME", link: "resume" },
        { name: "PROJECTS", link: "projects" }
    ]
    const [isOpen, setIsOpen] = useState(false)

    return (
        <div className='w-screen h-screen'>
            <div className='shadow-md w-full fixed top-0 left-0'>
                <div className=' bg-white md:px-10 py-7 px-2 md:flex  items-center justify-between'>
                    <div className='flex items-center gap-1'>
                        <img src={Square} width={"20px"} />
                        <p className='font-serif'>
                            <span className='font-bold text-xl md:text-2xl cursor-pointer'>
                                <Link to={"/"}>
                                    Yulduzxon Maxammatyunusova
                                </Link>
                            </span>
                            <span className='font-sm'>
                                /Frontend Developer
                            </span>
                        </p>
                    </div>
                    <div onClick={() => setIsOpen(!isOpen)} className='w-7 h-7 absolute right-8 top-8 cursor-pointer md:hidden' >
                        {
                            isOpen ? <XMarkIcon /> : <Bars3BottomRightIcon />
                        }


                    </div>
                    <ul className={`md:flex pl-9 md:pl-0 md:items-center md:static md:pb-0 pb-12 absolute md:z-auto  z-[-1] left-0 md:w-auto w-full transition-all bg-white duration-500 ease-in ${isOpen ? 'top-14' : 'top-[-490px]'}`}>
                        {
                            Links.map((link) => (
                                <li className='text-sm font-medium my-7 md:my-0 md:ml-8 font-serif focus:text-blue'>
                                    <Link className='text-gray hover:text-blue-400 duration-300' to={link.link}>{link.name}</Link>
                                </li>
                            ))
                        }
                    </ul>
                </div>
            </div>
            <Outlet />
            <footer className='w-full fixed bottom-0 left-0'>
                <div className='bg-white md:px-10 py-7 px-2 flex-wrap md:flex items-center md:justify-between shadow-md'>
                    <div>
                        <p className='text-center font-light'>
                            © 2024 by Yulduz.
                        </p>
                    </div>
                    <div className='md:flex gap-9'>
                        <div>
                            <h5 className='text-center font-bold'>Call</h5>
                            <p className='text-center mt-1'>+998-90-765-25-06</p>
                        </div>
                        <div>
                            <h5 className='text-center font-bold'>Email</h5>
                            <p className='text-center mt-1'>yunusovayulduz85@gmail.com</p>
                        </div>
                        <div>
                            <h5 className='text-center font-bold'>Account</h5>
                            <div className='flex gap-3 justify-center mt-1'>
                                <Link to={"https://www.linkedin.com/in/yulduzxon-maxammatyunusova-991841290/"}>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-linkedin" viewBox="0 0 16 16">
                                        <path d="M0 1.146C0 .513.526 0 1.175 0h13.65C15.474 0 16 .513 16 1.146v13.708c0 .633-.526 1.146-1.175 1.146H1.175C.526 16 0 15.487 0 14.854zm4.943 12.248V6.169H2.542v7.225zm-1.2-8.212c.837 0 1.358-.554 1.358-1.248-.015-.709-.52-1.248-1.342-1.248S2.4 3.226 2.4 3.934c0 .694.521 1.248 1.327 1.248zm4.908 8.212V9.359c0-.216.016-.432.08-.586.173-.431.568-.878 1.232-.878.869 0 1.216.662 1.216 1.634v3.865h2.401V9.25c0-2.22-1.184-3.252-2.764-3.252-1.274 0-1.845.7-2.165 1.193v.025h-.016l.016-.025V6.169h-2.4c.03.678 0 7.225 0 7.225z" />
                                    </svg>
                                </Link>
                                <Link to={"https://gitlab.com/yunusovayulduz85"}>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-gitlab" viewBox="0 0 16 16">
                                        <path d="m15.734 6.1-.022-.058L13.534.358a.57.57 0 0 0-.563-.356.6.6 0 0 0-.328.122.6.6 0 0 0-.193.294l-1.47 4.499H5.025l-1.47-4.5A.572.572 0 0 0 2.47.358L.289 6.04l-.022.057A4.044 4.044 0 0 0 1.61 10.77l.007.006.02.014 3.318 2.485 1.64 1.242 1 .755a.67.67 0 0 0 .814 0l1-.755 1.64-1.242 3.338-2.5.009-.007a4.05 4.05 0 0 0 1.34-4.668Z" />
                                    </svg>
                                </Link>
                                <Link to={"https://t.me/MYD_05"}>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-telegram" viewBox="0 0 16 16">
                                        <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0M8.287 5.906q-1.168.486-4.666 2.01-.567.225-.595.442c-.03.243.275.339.69.47l.175.055c.408.133.958.288 1.243.294q.39.01.868-.32 3.269-2.206 3.374-2.23c.05-.012.12-.026.166.016s.042.12.037.141c-.03.129-1.227 1.241-1.846 1.817-.193.18-.33.307-.358.336a8 8 0 0 1-.188.186c-.38.366-.664.64.015 1.088.327.216.589.393.85.571.284.194.568.387.936.629q.14.092.27.187c.331.236.63.448.997.414.214-.02.435-.22.547-.82.265-1.417.786-4.486.906-5.751a1.4 1.4 0 0 0-.013-.315.34.34 0 0 0-.114-.217.53.53 0 0 0-.31-.093c-.3.005-.763.166-2.984 1.09" />
                                    </svg>
                                </Link>

                            </div>
                        </div>
                    </div>
                </div>

            </footer>
        </div>
    )
}

export default RootLayout