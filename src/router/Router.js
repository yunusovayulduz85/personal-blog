import React from 'react'
import { BrowserRouter, Route, Routes } from 'react-router-dom'
import RootLayout from '../layout/RootLayout'
import Resume from '../components/Resume'
import Projects from '../components/Projects'
import Home from '../components/Home'

const Router = () => {
    return (
        <div className='Router'>
            <BrowserRouter>
                <Routes>
                    <Route path='/' element={<RootLayout />}>
                        <Route index element={<Home />} />
                        <Route path='resume' element={<Resume />} />
                        <Route path='projects' element={<Projects />} />
                    </Route>
                </Routes>
            </BrowserRouter>
        </div>
    )
}

export default Router