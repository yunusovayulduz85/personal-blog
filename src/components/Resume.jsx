import React from 'react'
import Square from "../assets/square.png"
import ResumeImg from "../assets/resume.jpg"
const Resume = () => {
    return (
        <div className='max-w-screen bg-mainBg py-36'>
            <div className='flex justify-center gap-2 items-center'>
                <img src={Square} width={"20px"} height={"20px"} />
                <h4 className='font-bold text-2xl font-serif'>RESUME</h4>
            </div>
            <div className='px-72'>
                <div className='flex items-center justify-between mt-10'>
                    <h4 className='font-bold text-2xl font-serif'>Experience</h4>
                    <a href={ResumeImg} download >
                        <button className='btn btn-primary'>Download CV</button>
                    </a>
                </div>
                <div className='w-full bg-white mt-6 px-8 py-12 shadow-lg flex items-center justify-between'>
                    <div className=''>
                        <h4 className='font-extrabold text-blue-600 font-serif' >2024 - PRESENT</h4>
                        <h3 className='font-semibold font-serif mt-1'>Frontend Developer</h3>
                        <h4 className='flex gap-1 mt-2 font-bold'>
                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6">
                                <path stroke-linecap="round" stroke-linejoin="round" d="M2.25 21h19.5m-18-18v18m10.5-18v18m6-13.5V21M6.75 6.75h.75m-.75 3h.75m-.75 3h.75m3-6h.75m-.75 3h.75m-.75 3h.75M6.75 21v-3.375c0-.621.504-1.125 1.125-1.125h2.25c.621 0 1.125.504 1.125 1.125V21M3 3h12m-.75 4.5H21m-3.75 3.75h.008v.008h-.008v-.008Zm0 3h.008v.008h-.008v-.008Zm0 3h.008v.008h-.008v-.008Z" />
                            </svg>
                            AL Raqam Company
                        </h4>

                        <h4 className='flex mt-1 gap-1 font-serif'>
                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6">
                                <path stroke-linecap="round" stroke-linejoin="round" d="M15 10.5a3 3 0 1 1-6 0 3 3 0 0 1 6 0Z" />
                                <path stroke-linecap="round" stroke-linejoin="round" d="M19.5 10.5c0 7.142-7.5 11.25-7.5 11.25S4.5 17.642 4.5 10.5a7.5 7.5 0 1 1 15 0Z" />
                            </svg>
                            Yunusobod,Tashkent,Uzbekistan
                        </h4>
                    </div>
                    <div>
                        <p>
                            I was recently accepted to the company.<br /> For now, it is a trial
                            period, not an official job. <br />If I am lucky, I will increase my
                            experience in real <br /> projects and try to become an employee
                            of the Company!
                        </p>
                    </div>
                </div>
                <h4 className='font-bold text-2xl font-serif mt-10'>Education</h4>
                <div className='w-full bg-white mt-6 px-8 py-12 shadow-lg flex items-center justify-between'>
                    <div>
                        <h4 className='font-extrabold text-blue-600 font-serif'>2022-2026</h4>
                        <h4 className='font-serif font-semibold mt-1'>Software engineering</h4>
                        <h3 className='flex gap-1 font-bold mt-2'>
                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6">
                                <path stroke-linecap="round" stroke-linejoin="round" d="M3.75 21h16.5M4.5 3h15M5.25 3v18m13.5-18v18M9 6.75h1.5m-1.5 3h1.5m-1.5 3h1.5m3-6H15m-1.5 3H15m-1.5 3H15M9 21v-3.375c0-.621.504-1.125 1.125-1.125h3.75c.621 0 1.125.504 1.125 1.125V21" />
                            </svg>

                            Tashkent University of Information Technologies
                        </h3>
                        <h4 className='flex mt-1 gap-1 font-serif'>
                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6">
                                <path stroke-linecap="round" stroke-linejoin="round" d="M15 10.5a3 3 0 1 1-6 0 3 3 0 0 1 6 0Z" />
                                <path stroke-linecap="round" stroke-linejoin="round" d="M19.5 10.5c0 7.142-7.5 11.25-7.5 11.25S4.5 17.642 4.5 10.5a7.5 7.5 0 1 1 15 0Z" />
                            </svg>
                            Yunusobod,Tashkent,Uzbekistan
                        </h4>
                    </div>
                    <div>
                        <p>The university gave me a lot of acquaintances and <br /> communication, I can say that it is because of the <br /> university that I became more interested in programming.</p>
                    </div>
                </div>

                <div className='w-full bg-white mt-6 px-8 py-12 shadow-lg flex items-center justify-between'>
                    <div>
                        <h4 className='font-extrabold text-blue-600 font-serif'>2022-2024(now)</h4>
                        <h4 className='font-serif font-semibold mt-1'>Frontend</h4>
                        <h3 className='flex gap-1 font-bold mt-2'>
                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6">
                                <path stroke-linecap="round" stroke-linejoin="round" d="M3.75 21h16.5M4.5 3h15M5.25 3v18m13.5-18v18M9 6.75h1.5m-1.5 3h1.5m-1.5 3h1.5m3-6H15m-1.5 3H15m-1.5 3H15M9 21v-3.375c0-.621.504-1.125 1.125-1.125h3.75c.621 0 1.125.504 1.125 1.125V21" />
                            </svg>

                            Personal Development Process (PDP) Academy
                        </h3>
                        <h4 className='flex mt-1 gap-1 font-serif'>
                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6">
                                <path stroke-linecap="round" stroke-linejoin="round" d="M15 10.5a3 3 0 1 1-6 0 3 3 0 0 1 6 0Z" />
                                <path stroke-linecap="round" stroke-linejoin="round" d="M19.5 10.5c0 7.142-7.5 11.25-7.5 11.25S4.5 17.642 4.5 10.5a7.5 7.5 0 1 1 15 0Z" />
                            </svg>
                            Mirzo Ulugbek,Tashkent,Uzbekistan
                        </h4>
                    </div>
                    <div>
                        <p>
                            I think the biggest favor I have done for myself is <br /> that I chose PDP Academy, because I learned a lot there,<br /> my thinking has grown a lot!
                        </p>
                    </div>
                </div>
                <h4 className='font-bold text-2xl font-serif mb-6 mt-6'>Skills</h4>
                <div className='w-full bg-white px-8 py-12 shadow-lg items-center mt-6'>
                    <div className='flex items-center justify-between'>
                        <ul className='font-serif text-semibold' style={{ listStyleType: 'circle' }}>
                            <li className='font-semibold'>HTML5</li>
                            <li className='font-semibold'>CSS3</li>
                            <li className='font-semibold'>SASS</li>
                        </ul>
                        <ul className='font-serif' style={{ listStyleType: 'circle' }}>
                            <li className='font-semibold'>Bootstrap</li>
                            <li className='font-semibold'>Tailwind</li>
                            <li className='font-semibold'>MUI</li>
                        </ul>
                        <ul className='font-serif' style={{ listStyleType: 'circle' }}>
                            <li className='font-semibold'>JavaScript</li>
                            <li className='font-semibold'>Rect.js</li>
                            <li className='font-semibold'>Redux</li>
                        </ul>
                        <ul className='font-serif' style={{ listStyleType: 'circle' }}>
                            <li className='font-semibold'>Redux toolkit</li>
                            <li className='font-semibold'>Typescript</li>
                            <li className='font-semibold'>Next.js</li>
                        </ul>
                        <ul className='font-serif' style={{ listStyleType: 'circle' }}>
                            <li className='font-serif font-bold text-xl'>Language : </li>
                            <li className='font-semibold'>English (level A2)</li>
                            <li className='font-semibold'>Uzbek Native</li>
                        </ul>
                    </div>
                </div>
            </div>

        </div>
    )
}

export default Resume