import React from 'react'
import Square from "../assets/square.png"
import PaymentBill from "../assets/paymentBill.png"
import QuizTest2 from "../assets/quiztest2.png"
import Pizza from "../assets/pizza.png"
import OnlineShop from "../assets/online shop1.png"
import Courses from "../assets/language courses1.png"
import Courses2 from "../assets/language courses2.png"
const Projects = () => {
  return (
    <div className='w-screen py-36 bg-mainBg'>
      <div className='flex justify-center gap-2 items-center'>
        <img src={Square} width={"20px"} height={"20px"} />
        <h4 className='font-bold text-2xl font-serif'>PROJECTS</h4>
      </div>
      <div className='px-72'>
        <div className='flex items-center justify-between bg-white shadow-lg mt-5 px-5 py-2'>
          <div>
            <p className='font-serif'>
              One of my initial JavaScript learning projects aimed at <br /> automating loan origination, if we expand this project <br />it will definitely be a convenient solution <br /> for big business owners!
            </p>
          </div>
          <div>
            <img width={"500px"} src={PaymentBill} />
          </div>
        </div>
        <div className='flex items-center justify-between bg-white shadow-lg mt-5 px-5 py-2'>
          <div>
            <p className='font-serif'>
              This is my first project with React.js.<br /> The purpose of this project is to assess student <br /> learning through tests!
            </p>
          </div>
          <div className='flex gap-1'>
            <img width={"500px"} src={QuizTest2} alt='quiz tests image' />
          </div>
        </div>
        <div className='flex items-center justify-between bg-white shadow-lg mt-5 px-5 py-2'>
          <div>
            <p className='font-serif'>
              This is a site to buy pizza! The site is very user friendly!
            </p>
          </div>
          <div>
            <img width={"500px"} src={Pizza} />
          </div>
        </div>
        <div className='flex items-center justify-between bg-white shadow-lg mt-5 px-5 py-2'>
          <div>
            <p className='font-serif'>
              This is an online store project. <br />( Real projects can't be left behind, right😀?)
            </p>
          </div>
          <div>
            <img width={"500px"} src={OnlineShop} />
          </div>
        </div>
        <div className='flex items-center justify-between bg-white shadow-lg mt-5 px-5 py-2'>
          <div>
            <p className='font-serif'>
              This is a project I made with Typescript and MUI.<br /> The goal of the project is to ensure that the user <br /> learns any language easily and quickly!
            </p>
          </div>
          <div className=''>
            <img width={"500px"} src={Courses} className='mb-3' />
            <img width={"500px"} src={Courses2} />
          </div>
        </div>
      </div>
    </div>
  )
}

export default Projects