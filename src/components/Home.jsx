import React from 'react'
import { Link } from 'react-router-dom'

const Home = () => {
  return (
    <>
      <div className='w-full h-full  flex justify-center'>
        <div className='bg-mainBg w-6/12'></div>
        <div className='bg-white w-6/12'></div>
      </div>
      <div className='w-5/12 h-96 bg-white shadow-lg absolute md:mt-top md:ml-left rounded-lg'>
        <div className='px-8 py-6'>
          <h3 className='text-center font-bold text-2xl font-serif'>Assalomu alaykum!</h3>
          <p className='text-center text-xl font-serif mt-2'>My name is Yulduz, I am 19 years old</p>
          <p className='text-center text-xl font-serif mt-1'>My profession is Frontend developer</p>
          <p className='text-center text-xl font-serif mt-1'>Current activity is at the AL-Raqam company (Remote)</p>
        </div>
        <div className='flex justify-center gap-2'>
          <Link to={"resume"}>
            <button className='transition ease-in-out delay-150 duration-300 btn btn-primary hover:bg-white hover:text-black'>
              RESUME
            </button>
          </Link>
          <button className='transition ease-in-out delay-150 duration-300 btn font-semibold px-4 rounded-md hover:bg-blue-500 hover:text-white shadow-md'>
            <Link to={"projects"}>
              PROJECTS
            </Link>
          </button>
        </div>
        <div className='flex justify-center mt-5 gap-5'>
          <Link to={"https://www.linkedin.com/in/yulduzxon-maxammatyunusova-991841290/"}>
            <div className='rounded-full border-2 p-2 border-black'>
              <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-linkedin" viewBox="0 0 16 16">
                <path d="M0 1.146C0 .513.526 0 1.175 0h13.65C15.474 0 16 .513 16 1.146v13.708c0 .633-.526 1.146-1.175 1.146H1.175C.526 16 0 15.487 0 14.854zm4.943 12.248V6.169H2.542v7.225zm-1.2-8.212c.837 0 1.358-.554 1.358-1.248-.015-.709-.52-1.248-1.342-1.248S2.4 3.226 2.4 3.934c0 .694.521 1.248 1.327 1.248zm4.908 8.212V9.359c0-.216.016-.432.08-.586.173-.431.568-.878 1.232-.878.869 0 1.216.662 1.216 1.634v3.865h2.401V9.25c0-2.22-1.184-3.252-2.764-3.252-1.274 0-1.845.7-2.165 1.193v.025h-.016l.016-.025V6.169h-2.4c.03.678 0 7.225 0 7.225z" />
              </svg>
            </div>

          </Link>
          <Link to={"https://gitlab.com/yunusovayulduz85"}>
            <div className='rounded-full border-2 p-2 border-black' >
              <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-gitlab" viewBox="0 0 16 16">
                <path d="m15.734 6.1-.022-.058L13.534.358a.57.57 0 0 0-.563-.356.6.6 0 0 0-.328.122.6.6 0 0 0-.193.294l-1.47 4.499H5.025l-1.47-4.5A.572.572 0 0 0 2.47.358L.289 6.04l-.022.057A4.044 4.044 0 0 0 1.61 10.77l.007.006.02.014 3.318 2.485 1.64 1.242 1 .755a.67.67 0 0 0 .814 0l1-.755 1.64-1.242 3.338-2.5.009-.007a4.05 4.05 0 0 0 1.34-4.668Z" />
              </svg>
            </div>

          </Link>
          <Link to={"https://t.me/MYD_05"}>
            <div className='rounded-full border-2 p-2 border-black'>
              <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-telegram" viewBox="0 0 16 16">
                <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0M8.287 5.906q-1.168.486-4.666 2.01-.567.225-.595.442c-.03.243.275.339.69.47l.175.055c.408.133.958.288 1.243.294q.39.01.868-.32 3.269-2.206 3.374-2.23c.05-.012.12-.026.166.016s.042.12.037.141c-.03.129-1.227 1.241-1.846 1.817-.193.18-.33.307-.358.336a8 8 0 0 1-.188.186c-.38.366-.664.64.015 1.088.327.216.589.393.85.571.284.194.568.387.936.629q.14.092.27.187c.331.236.63.448.997.414.214-.02.435-.22.547-.82.265-1.417.786-4.486.906-5.751a1.4 1.4 0 0 0-.013-.315.34.34 0 0 0-.114-.217.53.53 0 0 0-.31-.093c-.3.005-.763.166-2.984 1.09" />
              </svg>
            </div>

          </Link>
        </div>
      </div>

    </>

  )
}

export default Home