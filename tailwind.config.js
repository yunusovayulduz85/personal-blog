/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
    extend: {
      backgroundColor: {
        mainBg: '#E6DACE'
      },
      margin: {
        left: "30%",
        top: "-38%",
      }
    },
  },
  plugins: [],
}

